/*
 * File: error.cpp
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 09/01/2016
 */

#include <cstdlib>
#include <windows.h>
#include <string>

#include "error.h"

namespace	PSDKLauncher
{
	int	error()
	{
		DWORD	id;
		LPWSTR	buffer;

		id = GetLastError();
		if (id == 0) return (error(TEXT("???")));
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS, NULL, id,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&buffer, 0,
				NULL);
		MessageBox(NULL, buffer, TEXT("Error"), MB_OK);
		LocalFree(buffer);
		return (EXIT_FAILURE);
	}

	int	error(std::wstring text)
	{
		MessageBox(NULL, text.c_str(), TEXT("Error"), MB_OK);
		return (EXIT_FAILURE);
	}
};
