/*
 * File: http.h
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 16/02/2016
 */

#ifndef HTTP_H_
# define HTTP_H_

# include <windows.h>
# include <wininet.h>
# include <fstream>
# include <map>

namespace	PSDKLauncher
{
	typedef std::map<std::string, std::string>		dict_t;
	typedef std::map<std::string, std::string>::iterator	dict_iter_t;

	class	Http
	{
		private:
			HINTERNET	internet;
			HINTERNET	file_downloader;
			std::ofstream	file_stream;
		public:
			std::string	content;

			Http();
			std::string	urlencode(std::string data);
			bool		get(LPWSTR host, LPWSTR dest);
			bool		post(LPWSTR host, LPWSTR form_act, dict_t data);
			bool		download(LPWSTR url, LPWSTR out, std::ios_base::openmode mode = std::ios_base::out);
			int		file_size(LPWSTR host, LPWSTR path);
			int		download_receive();
			void		download_finish();
			dict_t		parse(std::string content);
			~Http();
	};
};

#endif /* !HTTP_H_ */
