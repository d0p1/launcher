/*
 * File: main.cpp
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 16/02/2016
 */

#include <cstdlib>
#include <windows.h>

#include "gui.h"

int CALLBACK	WinMain(HINSTANCE current, HINSTANCE previous, LPSTR command,
			int optn)
{
	PSDKLauncher::Gui	window(current);

	window.show();
	window.loop();
	return (EXIT_SUCCESS);
}
