/*
 * File: system.h
 * Author: d0p1
 * Created on: 14/01/2016
 * Modified on: 17/01/2016
 */

#ifndef SYSTEM_H_
# define SYSTEM_H_

# include <shlobj.h>
# include <string>
# include <map>

# include "http.h"

namespace	PSDKLauncher
{
	class	System
	{
		private:
			wchar_t	path_to_ini[MAX_PATH] = { '\0' };

			void		get_ini_path();
			bool		file_exist(wchar_t *str);
			void		load_keys(dict_t keys);
			bool		check_file(std::string name, std::string hash);
			dict_t		parse(std::string line);
		public:
			std::string	version;
			bool		first_use;
			dict_t		keys;
			wchar_t		path[MAX_PATH] = { '\0' };

			System();
			void			set_path(wchar_t *u_path);
			dict_t			read_list();
			dict_t			read_file(dict_t keys);
			static std::wstring	to_w(std::string str);
			static std::string	utf8_encode(wchar_t *str);
			~System();
	};
};

#endif /* !SYSTEM_H_ */
