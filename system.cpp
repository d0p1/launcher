/*
 * File: system.cpp
 * Author: d0p1
 * Created on: 14/02/2016
 * Modified on: 17/02/2016
 */

#include <shlobj.h>
#include <shlwapi.h>
#include <io.h>
#include <fstream>
#include <algorithm>

#include "system.h"

namespace	PSDKLauncher
{
	System::System()
	{
		this->version = "";
		this->first_use = true;
		this->get_ini_path();
		if (this->file_exist(this->path_to_ini))
		{
			GetPrivateProfileString(TEXT("GLOBAL"), TEXT("path"), NULL, this->path,
					MAX_PATH, this->path_to_ini);
			if (wcsnlen(this->path, MAX_PATH) && this->file_exist(this->path))
			{
				this->load_keys(this->read_list());
				this->first_use = false;
			}
		}
	}

	void	System::set_path(wchar_t *u_path)
	{
		WritePrivateProfileStringW(NULL, NULL, NULL, this->path_to_ini);
		wcscpy_s(this->path, MAX_PATH, u_path);
		WritePrivateProfileStringW(TEXT("GLOBAL"), TEXT("path"), this->path,
				this->path_to_ini);
	}

	void	System::get_ini_path()
	{
		if (!SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0,
						this->path_to_ini)))
			wcscpy_s(this->path_to_ini, MAX_PATH, TEXT(".//"));
		PathAppend(this->path_to_ini, TEXT("psdk_launcher.ini"));
	}

	bool	System::file_exist(wchar_t *str)
	{
		return (_waccess(str, 0) == 0);
	}

	std::string	System::utf8_encode(wchar_t *str)
	{
		int		size;
		std::string	value;

		if (str == NULL)
			return (value);
		size = WideCharToMultiByte(CP_UTF8, 0, str, wcslen(str), NULL, 0, NULL, NULL);
		value = std::string(size, '\0');
		WideCharToMultiByte(CP_UTF8, 0, str, wcslen(str), &value[0], size, NULL, NULL);
		return (value);
	}

	dict_t	System::parse(std::string line)
	{
		dict_t		data;
		std::string	word("");
		std::string	header("");
		unsigned int	i;

		for (i = 0; i < line.size(); i++)
		{
			if (line[i] == ':')
			{
				header = word;
				word = "";
			}
			else
			{
				word += line[i];
			}
		}
		if (word != "" && header != "")
			data[header] = word;
		return (data);
	}

	dict_t	System::read_list()
	{
		std::ifstream	file_stream;
		dict_t		keys_files;
		dict_t		tmp;
		wchar_t		list_path[MAX_PATH] = { '\0' };
		std::string	line;

		wcscpy_s(list_path, MAX_PATH, this->path);
		PathAppend(list_path, TEXT("list.txt"));
		file_stream.open(list_path);
		if (file_stream.is_open())
		{
			while (std::getline(file_stream, line))
			{
				tmp = this->parse(line);
				keys_files.insert(tmp.begin(), tmp.end());
			}
			file_stream.close();
		}
		return (keys_files);
	}

	void	System::load_keys(dict_t keys)
	{
		std::ifstream	file_stream;
		dict_iter_t	iter;
		dict_t		tmp;
		wchar_t		keys_path[MAX_PATH] = { '\0' };
		std::string	tmp_str;
		std::wstring	tmp_wchar;
		std::string	line;

		for (iter = keys.begin(); iter != keys.end(); iter++)
		{
			tmp_str = iter->second;
			std::replace(tmp_str.begin(), tmp_str.end(), '/', '\\');
			tmp_wchar = std::wstring(tmp_str.begin(), tmp_str.end()) +
				std::wstring(iter->first.begin(), iter->first.end());
			ZeroMemory(keys_path, MAX_PATH);
			wcscpy_s(keys_path, MAX_PATH, this->path);
			PathAppend(keys_path, tmp_wchar.c_str());
			file_stream.open(keys_path);
			if (file_stream.is_open())
			{
				while (std::getline(file_stream, line))
				{
					tmp = this->parse(line);
					this->keys[iter->second + tmp.begin()->first] = tmp.begin()->second;
				}
				file_stream.close();
			}

		}

	}

	bool	System::check_file(std::string name, std::string hash)
	{
		if (this->keys.find(name) == this->keys.end())
			return (true);
		if (this->keys[name] != hash)
			return (true);
		return (false);
	}

	dict_t	System::read_file(dict_t keys)
	{
		std::ifstream	file_stream;
		dict_iter_t	iter;
		dict_t		result;
		dict_t		tmp;
		wchar_t		keys_path[MAX_PATH] = { '\0' };
		std::string	tmp_str;
		std::wstring	tmp_wchar;
		std::string	line;

		for (iter = keys.begin(); iter != keys.end(); iter++)
		{
			tmp_str = iter->second;
			std::replace(tmp_str.begin(), tmp_str.end(), '/', '\\');
			tmp_wchar = std::wstring(tmp_str.begin(), tmp_str.end()) +
				std::wstring(iter->first.begin(), iter->first.end());
			ZeroMemory(keys_path, MAX_PATH);
			wcscpy_s(keys_path, MAX_PATH, this->path);
			PathAppend(keys_path, tmp_wchar.c_str());
			file_stream.open(keys_path);
			if (file_stream.is_open())
			{
				while (std::getline(file_stream, line))
				{
					tmp = this->parse(line);
					if (this->check_file(iter->second + tmp.begin()->first,
								tmp.begin()->second))
					{
						result[tmp.begin()->first] = iter->second;
					}
				}
				file_stream.close();
			}
		}
		return (result);
	}

	std::wstring	System::to_w(std::string str)
	{
		return (std::wstring(str.begin(), str.end()));
	}

	System::~System()
	{
	}
};
