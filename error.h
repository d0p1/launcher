/*
 * File: error.h
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 09/01/2016
 */

#ifndef ERROR_H_
# define ERROR_H_

# include <windows.h>
# include <string>

namespace	PSDKLauncher
{
	int	error();
	int	error(std::wstring text);
};

#endif /* !ERROR_H_ */
