/*
 * File: gui.cpp
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 16/02/2016
 */

#include <cstdlib>
#include <windows.h>
#include <commctrl.h>
#include <shlwapi.h>
#include <thread>

#include "gui.h"
#include "install.h"
#include "http.h"
#include "error.h"
#include "crypto.h"
#include "resource.h"

#pragma comment(linker, \
		  "\"/manifestdependency:type='Win32' "\
		  "name='Microsoft.Windows.Common-Controls' "\
		  "version='6.0.0.0' "\
		  "processorArchitecture='*' "\
		  "publicKeyToken='6595b64144ccf1df' "\
		  "language='*'\"")

namespace	PSDKLauncher
{
	HINSTANCE	Gui::instance = NULL;
	Http		Gui::http;
	dict_t		Gui::serv_info;
	System		Gui::client_info;
	bool		Gui::has_internet = true;

	Gui::Gui(HINSTANCE current)
	{
		Gui::instance = current;
		Gui::http.get(TEXT("communityscriptproject.com"), TEXT("/PSDK/check_version.php"));
		if (Gui::http.content != "")
		{
			Gui::serv_info = Gui::http.parse(Gui::http.content);
			if (Gui::serv_info.find("sid") == Gui::serv_info.end() &&
					Gui::serv_info.find("log") == Gui::serv_info.end())
				Gui::has_internet = false;
		}
		else
		{
			Gui::has_internet = false;
		}
		InitCommonControls();
		if (Gui::client_info.first_use)
		{
			Install	install(Gui::instance);
			Gui::client_info.set_path(install.path);
		}
		this->window = CreateDialogParam(Gui::instance, MAKEINTRESOURCE(IDD_DIALOG1), 0,
				Gui::procedure, 0);
	}

	INT_PTR CALLBACK	Gui::procedure(HWND window, UINT state, WPARAM wparam, LPARAM lparam)
	{
		wchar_t	game_exe[MAX_PATH] = { '\0' };

		switch (state)
		{
			case WM_INITDIALOG:
				Gui::create_content(window);
				return (TRUE);
			case WM_COMMAND:
				switch (LOWORD(wparam))
				{
					case IDC_BUTTON_CONNECT:
						Gui::perform_login(window);
						break;
					case IDC_BUTTON_START:
						wcscpy_s(game_exe, MAX_PATH, Gui::client_info.path);
						PathAppend(game_exe, TEXT("game.exe"));
						ShellExecute(NULL, NULL, game_exe, NULL, NULL, SW_SHOW);
						EndDialog(window, 0);
						exit(EXIT_SUCCESS);
						return (TRUE);
				}
				return (TRUE);
			case WM_CLOSE:
				DestroyWindow(window);
				return (TRUE);
			case WM_DESTROY:
				PostQuitMessage(0);
				return (TRUE);
			default:
				return (FALSE);
		}
	}

	void	Gui::perform_login(HWND base_window)
	{
		char		username[100] = { '\0' };
		char		password[100] = { '\0' };
		std::thread	action;

		GetWindowTextA(GetDlgItem(base_window, IDC_PASSWORD), password, 100);
		GetWindowTextA(GetDlgItem(base_window, IDC_USERNAME), username, 100);
		if (strnlen(username, 100) != 0 && strnlen(password, 100) != 0)
		{
			SendMessage(GetDlgItem(base_window, IDC_USERNAME), EM_SETREADONLY, 1, 0);
			SendMessage(GetDlgItem(base_window, IDC_PASSWORD), EM_SETREADONLY, 1, 0);
			EnableWindow(GetDlgItem(base_window, IDC_BUTTON_CONNECT), FALSE);
			action = std::thread(Gui::action_login_thread, base_window, _strdup(username), _strdup(password));
			action.detach();
		}
		else
		{
			MessageBox(base_window, TEXT("Veuillez remplir tous les champs"), TEXT("Erreur"), 0);
		}
	}

	void Gui::action_login_thread(HWND base_window, char *username, char *password)
	{
		dict_t		params;
		unsigned int	i;

		if (!Gui::has_internet)
		{
			if (Gui::client_info.version == "")
			{
				MessageBox(base_window, TEXT("Can't reach server"), TEXT("Erreur"), 0);
			}
			else
			{
				Gui::show_log(base_window);
				Gui::log_append(GetDlgItem(base_window, IDC_LOG_INFO),
						"Can't reach server\n");
				EnableWindow(GetDlgItem(base_window, IDC_BUTTON_START), TRUE);
			}
			return;
		}
		params["user"] = username;
		for (i = 0; i < params["user"].size(); i++)
			params["user"][i] = tolower(params["user"][i]);
		params["passwrd"] = password;
		params["hash_passwrd"] = Sha1((params["user"] + params["passwrd"]).c_str()).hexdigest();
		params["hash_passwrd"] = Sha1((params["hash_passwrd"] + Gui::serv_info["sid"]).c_str()).hexdigest();
		Gui::http.post(TEXT("communityscriptproject.com"), TEXT("/forum/index.php?action=login2"),
				params);
		free(username);
		free(password);
		Gui::http.get(TEXT("communityscriptproject.com"), TEXT("/PSDK/check_version.php"));
		Gui::serv_info = Gui::http.parse(Gui::http.content);
		if (Gui::serv_info.find("log") == Gui::serv_info.end())
		{
			MessageBox(base_window, TEXT("Incorrect login or password"), TEXT("Erreur"), 0);
			SendMessage(GetDlgItem(base_window, IDC_USERNAME), EM_SETREADONLY, 0, 0);
			SendMessage(GetDlgItem(base_window, IDC_PASSWORD), EM_SETREADONLY, 0, 0);
			EnableWindow(GetDlgItem(base_window, IDC_BUTTON_CONNECT), TRUE);
			return;
		}
		Gui::show_log(base_window);
		Gui::action_check_thread(base_window);
	}

	void	Gui::action_check_thread(HWND base_window)
	{
		HWND		log;
		dict_t		keys;
		dict_t		files;
		dict_iter_t	iter;

		log = GetDlgItem(base_window, IDC_LOG_INFO);
		if (Gui::client_info.version != "")
			Gui::log_append(log, "Current version: " + Gui::client_info.version + "\n");
		Gui::log_append(log, "Internet version: " + Gui::serv_info["version"] + "\n");
		if (Gui::client_info.version == Gui::serv_info["version"])
		{
			Gui::log_append(log, "Client is up to date :)\n");
			EnableWindow(GetDlgItem(base_window, IDC_BUTTON_START), TRUE);
			return;
		}
		Gui::log_append(log, "Updating client, this may take a while\n");
		Gui::log_append(log, "Getting file list\n");
		if (!Gui::download_thread(base_window, TEXT("/PSDK/Release/list.txt"),
					std::wstring(std::wstring(Gui::client_info.path)
					+ L"\\list.txt").c_str()))
		{
			Gui::log_append(log, "Error can't get files list ><\n");
			if (Gui::client_info.version != "")
				EnableWindow(GetDlgItem(base_window, IDC_BUTTON_START), TRUE);
			return;
		}
		keys = Gui::client_info.read_list();
		for (iter = keys.begin(); iter != keys.end(); iter++)
		{
			Gui::log_append(log, "Checking: " + iter->first  + "\n");
			Gui::download_thread(base_window, std::wstring(L"/PSDK/Release"
					+ System::to_w(iter->second + iter->first)).c_str(),
					std::wstring(std::wstring(Gui::client_info.path)
					+ System::to_w(iter->second + iter->first)).c_str());
		}
		files = Gui::client_info.read_file(keys);
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_TOTAL), PBM_SETRANGE32, 0, files.size());
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_TOTAL), PBM_SETSTEP, 1, 0);
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_TOTAL), PBM_SETPOS, 0, 0);
		for (iter = files.begin(); iter != files.end(); iter++)
		{
			Gui::log_append(log, "Downloading " + iter->first + "\n");
			Gui::download_thread(base_window, std::wstring(L"/PSDK/Release"
					+ System::to_w(iter->second + iter->first)).c_str(),
					std::wstring(std::wstring(Gui::client_info.path)
					+ System::to_w(iter->second + iter->first)).c_str());
			SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR), PBM_STEPIT, 0, 0);
		}
		EnableWindow(GetDlgItem(base_window, IDC_BUTTON_START), TRUE);
	}

	bool	Gui::download_thread(HWND base_window, std::wstring dest, std::wstring out)
	{
		int	size;
		int	byte_read;
		int	total_read;
		int	err;

		size = Gui::http.file_size(TEXT("communityscriptproject.com"), const_cast<wchar_t *>(dest.c_str()));
		if (size == -1)
			return (false);
		if (!Gui::http.download(const_cast<wchar_t *>((
					std::wstring(L"http://communityscriptproject.com") + dest).c_str()),
					const_cast<wchar_t *>(out.c_str()), std::ios_base::out | std::ios_base::binary))
			return (false);
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR), PBM_SETRANGE32, 0, size);
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR), PBM_SETSTEP, 1024, 0);
		SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR), PBM_SETPOS, 0, 0);
		total_read = 0;
		err = 0;
		while (size > total_read)
		{
			byte_read = Gui::http.download_receive();
			if (byte_read == 0)
			{
				err += 1;
				if (err >= 3)
				{
					Gui::http.download_finish();
					return (false);
				}
			}
			else
			{
				err = 0;
				total_read += byte_read;
				SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR),
						PBM_SETSTEP, byte_read, 0);
				SendMessage(GetDlgItem(base_window, IDC_PROGRESS_CURR),
						PBM_STEPIT, 0, 0);
			}
		}
		Gui::http.download_finish();
		return (true);
	}

	void	Gui::log_append(HWND log_win, std::string text)
	{
		int	start;
		int	end;
		int	size;

		SendMessageA(log_win, EM_GETSEL, reinterpret_cast<WPARAM>(&start),
				reinterpret_cast<LPARAM>(&end));
		size = GetWindowTextLengthA(log_win);
		SendMessageA(log_win, EM_SETSEL, size, size);
		SendMessageA(log_win, EM_REPLACESEL, TRUE, reinterpret_cast<LPARAM>(text.c_str()));
		SendMessageA(log_win, EM_SETSEL, start, end);
	}

	void	Gui::show_log(HWND base_window)
	{
		ShowWindow(GetDlgItem(base_window, IDC_USERNAME), SW_HIDE);
		ShowWindow(GetDlgItem(base_window, IDC_PASSWORD), SW_HIDE);
		ShowWindow(GetDlgItem(base_window, IDC_STATIC_USERNAME), SW_HIDE);
		ShowWindow(GetDlgItem(base_window, IDC_STATIC_PASSWORD), SW_HIDE);
		ShowWindow(GetDlgItem(base_window, IDC_BUTTON_CONNECT), SW_HIDE);
		ShowWindow(GetDlgItem(base_window, IDC_LOG_INFO), SW_SHOW);
		ShowWindow(GetDlgItem(base_window, IDC_BUTTON_START), SW_SHOW);
	}

	void	Gui::create_content(HWND base_window)
	{
		HICON		icon;
		std::thread	action;

		icon = LoadIcon(Gui::instance, MAKEINTRESOURCE(IDI_ICON));
		SendMessage(base_window, WM_SETICON, WPARAM(ICON_SMALL), LPARAM(icon));
		SetDlgItemText(base_window, IDC_VERSION, TEXT("Version: None"));
		if (Gui::serv_info.find("log") != Gui::serv_info.end())
		{
			Gui::show_log(base_window);
			action = std::thread(Gui::action_check_thread, base_window);
			action.detach();
		}
	}

	void	Gui::show()
	{
		ShowWindow(this->window, SW_SHOW);
		UpdateWindow(this->window);
	}

	void	Gui::loop()
	{
		MSG msg;

		while (GetMessage(&msg, NULL, 0, 0) > 0)
		{
			if (!IsDialogMessage(this->window, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	Gui::~Gui()
	{
	}
};
