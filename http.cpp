/*
 * File: http.cpp
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 16/02/2016
 */

#include <sstream>
#include <iomanip>
#include <cctype>

#include "http.h"
#include "error.h"

namespace	PSDKLauncher
{
	Http::Http()
	{
		this->content = "";
		this->file_downloader = NULL;
		this->internet = InternetOpen(TEXT("PSDK Launcher"), INTERNET_OPEN_TYPE_DIRECT,
				NULL, NULL, 0);
		if (this->internet == NULL)
			exit(error());
	}

	std::string	Http::urlencode(const std::string data)
	{
		std::ostringstream	encoded;
		unsigned int		i;
		char			c;

		encoded.fill('0');
		encoded << std::hex;
		for (i = 0; i != data.size(); i++)
		{
			c = data[i];
			if (std::isalnum(c) || c == '-' || c == '.' || c == '~')
			{
				encoded << c;
			}
			else
			{
				encoded << std::uppercase;
				encoded << '%' << std::setw(2) << int((unsigned char) c);
				encoded << std::nouppercase;
			}
		}	
		return (encoded.str());
	}

	bool	Http::post(LPWSTR host, LPWSTR form_act, dict_t data)
	{
		HINTERNET	conn;
		HINTERNET	request;
		char		*header = "Content-Type: application/x-www-form-urlencoded\r\n";
		char		page_buffer[512] = { '\0' };
		int		byte_read;
		dict_iter_t	iter;
		std::string	form_data("");

		conn = InternetConnect(this->internet, host, INTERNET_DEFAULT_HTTP_PORT, NULL,
				NULL, INTERNET_SERVICE_HTTP, 0, 1);
		if (conn == NULL)
			return (false);
		request = HttpOpenRequest(conn, TEXT("POST"), form_act, NULL, NULL, NULL, 0, 1);
		if (request == NULL)
		{
			InternetCloseHandle(conn);
			error();
			return (false);
		}
		for (iter = data.begin(); iter != data.end(); iter++)
		{
			if (form_data != "")
				form_data += "&";
			form_data += iter->first + "=" + this->urlencode(iter->second);
		}
		HttpSendRequestA(request, header, strlen(header),
				const_cast<char *>(form_data.c_str()), form_data.size());
		this->content = "";
		while (InternetReadFile(request, page_buffer, 500, (LPDWORD)&byte_read)
				&& byte_read > 0)
		{
			page_buffer[byte_read] = '\0';
			this->content += page_buffer;
		}
		InternetCloseHandle(request);
		InternetCloseHandle(conn);
		return (true);
	}

	bool	Http::get(LPWSTR host, LPWSTR dest)
	{
		HINTERNET	conn;
		HINTERNET	request;
		char		*header = "Content-Type: application/x-www-form-urlencoded\r\n";
		char		page_buffer[513] = { '\0' };
		int		byte_read;

		conn = InternetConnect(this->internet, host, INTERNET_DEFAULT_HTTP_PORT, NULL,
				NULL, INTERNET_SERVICE_HTTP, 0, 1);
		if (conn == NULL)
		{
			error();
			return (false);
		}
		request = HttpOpenRequest(conn, TEXT("GET"), dest, NULL, NULL, NULL, 0, 1);
		if (request == NULL)
		{
			error();
			InternetCloseHandle(conn);
			return (false);
		}
		HttpSendRequest(request, NULL, 0, NULL, 0);
		this->content = "";
		while (InternetReadFile(request, page_buffer, 512, (LPDWORD)&byte_read)
				&& byte_read > 0)
		{
			page_buffer[512] = '\0';
			this->content += page_buffer;
		}
		InternetCloseHandle(request);
		InternetCloseHandle(conn);
		return (true);
	}

	dict_t	Http::parse(std::string content)
	{
		unsigned int	i;
		dict_t		config;
		std::string	word;
		std::string	header;

		for (i = 0; i < content.size(); i++)
		{
			if (content[i] == '<')
			{
				if (header != "")
				{
					if (word == "")
						word = header;
					config[header] = word;
					word = "";
				}
			}
			else if (content[i] == '>')
			{
				if (word != "")
				{
					header = word;
					word = "";
				}
			}
			else
			{
				word += content[i];
			}
		}
		return (config);
	}

	int	Http::file_size(LPWSTR host, LPWSTR path)
	{
		HINTERNET	conn;
		HINTERNET	request;
		LPVOID		header_info;
		DWORD		header_size;
		int		ret;

		header_info = NULL;
		header_size = 0;
		ret = -1;
		conn = InternetConnect(this->internet, host, INTERNET_DEFAULT_HTTP_PORT, NULL,
				NULL, INTERNET_SERVICE_HTTP, 0, 0);
		if (conn == NULL)
		{
			error();
			return (-1);
		}
		request = HttpOpenRequest(conn, TEXT("GET"), path, NULL, NULL, NULL, 0, 1);
		if (request == NULL)
		{
			InternetCloseHandle(conn);
			error();
			return (-1);
		}
		HttpSendRequest(request, NULL, 0, NULL, 0);
		while (!HttpQueryInfo(request, HTTP_QUERY_CONTENT_LENGTH, header_info,
					&header_size, NULL))
		{
			if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
			{
				if (header_info != NULL)
					delete[] header_info;
				InternetCloseHandle(request);
				InternetCloseHandle(conn);
				error();
				return (-1);
			}
			header_info = new char[header_size];
		}
		if (header_info == NULL)
		{
			error(L"Can't get size");
			InternetCloseHandle(request);
			InternetCloseHandle(conn);
			return (-1);
		}
		ret = _wtoi((LPWSTR)header_info);
		delete[] header_info;
		InternetCloseHandle(request);
		InternetCloseHandle(conn);
		return (ret);
	}

	bool	Http::download(LPWSTR url, LPWSTR out, std::ios_base::openmode mode)
	{
		this->file_downloader = InternetOpenUrl(this->internet, url, NULL, NULL,
				INTERNET_FLAG_DONT_CACHE, NULL);
		if (this->file_downloader == NULL)
		{
			error();
			return (false);
		}
		this->file_stream.open(out, mode);
		if (!this->file_stream.is_open())
		{
			InternetCloseHandle(this->file_downloader);
			error(L"Can't open " + std::wstring(out));
			return (false);
		}
		return (true);
	}

	int	Http::download_receive()
	{
		char	*buffer;
		int	byte_read;

		if (this->file_downloader == NULL || !this->file_stream.is_open())
			return (0);
		buffer = new char[1024];
		if (buffer == NULL)
		{
			error(L"Can't allocate memory :'( so sad");
			return (0);
		}
		ZeroMemory(buffer, 1024);
		InternetReadFile(this->file_downloader, (LPVOID)buffer, 1024, (LPDWORD)&byte_read);
		if (byte_read > 0)
		{

			this->file_stream.write(buffer, byte_read);
			this->file_stream.flush();
		}
		delete[] buffer;
		return (byte_read);
	}

	void	Http::download_finish()
	{
		if (this->file_stream.is_open())
			this->file_stream.close();
		if (this->file_downloader != NULL)
			InternetCloseHandle(this->file_downloader);
	}

	Http::~Http()
	{
		InternetCloseHandle(this->internet);
	}
};
