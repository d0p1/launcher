/*
 * File: install.cpp
 * Author: d0p1
 * Created on: 17/02/2016
 * Modified on: 17/02/2016
 */

#include <windows.h>
#include <shlobj.h>

#include "install.h"
#include "resource.h"

namespace	PSDKLauncher
{
	wchar_t	Install::path[MAX_PATH] = { '\0' };

	Install::Install(HINSTANCE instance)
	{
		DialogBox(instance, MAKEINTRESOURCE(IDD_DIALOG2), this->window, Install::procedure);
	}

	INT	Install::browser_callback(HWND window, UINT msg, LPARAM lparam, LPARAM data)
	{
		if (msg == BFFM_INITIALIZED)
			SendMessage(window, BFFM_SETSELECTION, TRUE, data);
		return (0);
	}

	INT_PTR	Install::procedure(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
	{
		switch (msg)
		{
			case WM_INITDIALOG:
				return (TRUE);
			case WM_COMMAND:
				switch (wparam)
				{
					case IDC_BUTTON_OK:
						if (wcsnlen(Install::path, MAX_PATH) != 0)
							EndDialog(window, 0);
						return (TRUE);
					case IDC_BUTTON_PATH:
						if (Install::get_folder(window, L"PSDK Path",
									L"C:\\"))
							SetWindowText(GetDlgItem(window, IDC_EDIT_PATH),
									Install::path);
						return (TRUE);
				}
				break;
			case WM_CLOSE:
				DestroyWindow(window);
				exit(EXIT_SUCCESS);
				return (TRUE);
		}
		return (FALSE);
	}

	bool	Install::get_folder(HWND current_window, LPWSTR title, LPWSTR path)
	{
		BROWSEINFO	binf;
		LPITEMIDLIST	pidl;

		ZeroMemory(&binf, sizeof(BROWSEINFO));
		binf.lpfn = Install::browser_callback;
		binf.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
		binf.hwndOwner = current_window;
		binf.lpszTitle = title;
		binf.lParam = (LPARAM)path;
		pidl = NULL;
		if ((pidl = SHBrowseForFolder(&binf)) != NULL)
		{
			if (SHGetPathFromIDList(pidl, Install::path))
				return (true);
		}
		return (false);
	}

	Install::~Install()
	{
	}
};
