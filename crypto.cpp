/*
 * File: crypto.cpp
 * Author: d0p1
 * Created on: 10/01/2016
 * Modified on: 10/01/2016
 */

#include <windows.h>
#include <wincrypt.h>
#include <sstream>

#include "crypto.h"
#include "error.h"

namespace	PSDKLauncher
{
	Sha1::Sha1(const char *text)
	{
		HCRYPTPROV	crypt_prov;
		HCRYPTHASH	crypt_hash;
		DWORD		size_len;

		if (!CryptAcquireContext(&crypt_prov, NULL, NULL, PROV_RSA_FULL,
					CRYPT_VERIFYCONTEXT))
			exit(error());
		if (!CryptCreateHash(crypt_prov, CALG_SHA, 0, 0, &crypt_hash))
		{
			CryptReleaseContext(crypt_prov, 0);
			exit(error());
		}
		if (!CryptHashData(crypt_hash, (BYTE *)text, strlen(text), 0))
		{
			CryptDestroyHash(crypt_hash);
			CryptReleaseContext(crypt_prov, 0);
			exit(error());
		}
		size_len = sizeof(DWORD);
		if (!CryptGetHashParam(crypt_hash, HP_HASHSIZE, (BYTE *)&this->hash_length,
					&size_len, 0))
		{
			CryptDestroyHash(crypt_hash);
			CryptReleaseContext(crypt_prov, 0);
			exit(error());
		}
		this->hash = new BYTE[this->hash_length + 1];
		if (!CryptGetHashParam(crypt_hash, HP_HASHVAL, this->hash, &this->hash_length, 0))
		{
			CryptDestroyHash(crypt_hash);
			CryptReleaseContext(crypt_prov, 0);
			delete[] this->hash;
			exit(error());
		}
		CryptDestroyHash(crypt_hash);
		CryptReleaseContext(crypt_prov, 0);
	}

	std::string	Sha1::hexdigest()
	{
		std::stringstream	stream;
		unsigned int		i;

		for (i = 0; i < this->hash_length; i++)
			stream << std::hex << (int)this->hash[i];
		return (stream.str());
	}

	Sha1::~Sha1()
	{
		if (this->hash != NULL)
			delete[] this->hash;
	}
};
