/*
 * File: Install.h
 * Author: d0p1
 * Created on: 17/02/2016
 * Modified on: 17/02/2016
 */

#ifndef INSTALL_H_
# define INSTALL_H_

# include <windows.h>

namespace	PSDKLauncher
{
	class	Install
	{
		private:
			HWND			window;

			static INT CALLBACK	browser_callback(HWND, UINT, LPARAM, LPARAM);
			static INT_PTR CALLBACK	procedure(HWND window, UINT msg, WPARAM wparam,
					LPARAM lparam);
			static bool		get_folder(HWND instance, LPWSTR title, LPWSTR path);
		public:
			static wchar_t	path[MAX_PATH];

			Install(HINSTANCE instance);
			~Install();
	};
};

#endif /* !INSTALL_H_ */
