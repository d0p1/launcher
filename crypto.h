/*
 * File: crypto.h
 * Author: d0p1
 * Created on: 10/01/2016
 * Modified on: 10/01/2016
 */

#ifndef CRYPTO_H_
# define CRYPTO_H_

# include <windows.h>
# include <string>

namespace	PSDKLauncher
{
	class	Sha1
	{
		private:
			DWORD	hash_length;
			BYTE	*hash = NULL;
		public:
			Sha1(const char *text);
			std::string hexdigest();
			~Sha1();
	};
};

#endif /* !CRYPTO_H_ */
