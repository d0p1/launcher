/*
 * File: gui.h
 * Author: d0p1
 * Created on: 08/01/2016
 * Modified on: 16/02/2016
 */

#ifndef GUI_H_
# define GUI_H_

# include <windows.h>
# include <string>

# include "http.h"
# include "system.h"

namespace	PSDKLauncher
{
	class	Gui
	{
		private:
			HWND			window;
			static HINSTANCE	instance;
			static Http		http;
			static dict_t		serv_info;
			static System		client_info;
			static bool		has_internet;

			static void	create_content(HWND base_window);
			static void	perform_login(HWND base_window);
			static void	action_login_thread(HWND base_window, char *username, char *password);
			static void	log_append(HWND log_win, std::string text);
			static void	action_check_thread(HWND base_window);
			static bool	download_thread(HWND base_window, std::wstring dest, std::wstring out);
			static void	show_log(HWND base_window);
		public:
			Gui(HINSTANCE current);
			static INT_PTR CALLBACK	procedure(HWND, UINT, WPARAM, LPARAM);
			void			show();
			void			loop();
			~Gui();
	};
};

#endif /* !GUI_H_ */
