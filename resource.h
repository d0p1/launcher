//{{NO_DEPENDENCIES}}
// fichier Include Microsoft Visual C++.
// Utilisé par PSDK_Launcher.rc
//
#define IDI_ICON                        101
#define IDD_DIALOG1                     102
#define IDD_DIALOG2                     104
#define IDC_PASSWORD                    1002
#define IDC_USERNAME                    1003
#define IDC_PROGRESS_TOTAL              1004
#define IDC_PROGRESS_CURR               1005
#define IDC_BUTTON_CONNECT              1006
#define IDC_VERSION                     1007
#define IDC_LOG_INFO                    1008
#define IDC_STATIC_PASSWORD             1009
#define IDC_STATIC_USERNAME             1010
#define IDC_BUTTON_START                1011
#define IDC_EDIT_PATH                   1012
#define IDC_BUTTON1                     1013
#define IDC_BUTTON_PATH                 1013
#define IDC_BUTTON_OK                   1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
