# Launcher

# Testé sur

| Système             | ok?      |
|---------------------|----------|
| Windows XP (32bit)  | OK       |
| Windows Vista       | Euh GTFO |
| Windows 7 (32bit)   | OK       |
| Windows 7 (64bit)   | OK       |
| Windows 8.1 (32bit) | OK       |
| Windows 8.1 (64bit) | OK       |
| Windows 10 (32bit)  | OK       |

## Instruction de compilation

### MSVC (2015)

Ajoutez ces libs:

* Wininet.lib
* Shlwapi.lib
* ComCtl32.lib

## Todo

* i18n
* documenter le code ([Doxygen](https://fr.wikipedia.org/wiki/Doxygen))
* auto-maj du launcher

## Bugs connus

* "Plus de données sont disponibles"

## Crédits

* d0p1 (code)
* Dakurei (debug)
